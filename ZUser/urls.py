from django.urls import path

from . import views

app_name = 'ZUser'

urlpatterns = [
    path('', views.index, name='index'), #name= url의 별칭추가 http://localhost:8000/pybo/ 을 index
]