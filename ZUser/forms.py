from django import forms
from django.forms.fields import Field
from ZUser.models import ZUser

class UserForm(forms.ModelForm):
    class Meta:
        model = ZUser  # 사용할 모델
        fields = ['user_code', 'name']  # QuestionForm에서 사용할 Question 모델의 속성
        # widgets = {
        #     'subject': forms.TextInput(attrs={'class': 'form-control'}),
        #     'content': forms.Textarea(attrs={'class': 'form-control', 'rows': 10}),
        # }