from django.db import models
# Create your models here.

class ZUser(models.Model):
    user_code = models.TextField()
    name = models.CharField(max_length=20)
    age = models.CharField(max_length=5)
    add_date = models.DateTimeField()
    email = models.TextField()
    
    def __str__(self):
        return self.name
    
    
    