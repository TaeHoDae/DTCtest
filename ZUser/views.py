from django.http.response import HttpResponse
from django.shortcuts import render

from ZUser.models import ZUser

# Create your views here.

def index(request):
    user_list = ZUser.objects.order_by('-add_date')
    context = {'user_list': user_list}
    return render(request, 'ZUser/user_list.html', context)