from django.apps import AppConfig


class ZsurveyCodeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ZSurvey_code'
