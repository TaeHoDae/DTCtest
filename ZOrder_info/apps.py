from django.apps import AppConfig


class ZorderInfoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ZOrder_info'
